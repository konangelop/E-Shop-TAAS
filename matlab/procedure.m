clear; clc;

file_fun = 'init_mpc.py';
%% Import data
sampling_period = 5;
filename = 'data/sys_id_output12.csv';
[VM,cores,ads,courier,mode,responseTime,revenue,cost,loadSR,deliverySR,satisfaction,comparison] = importfile_eshop(filename);
VMy = [1;VM(1:end-1)];
data = iddata([responseTime,cost],[VM,cores,ads,mode], sampling_period);
data.InputName  = {'VM';'Cores';'Ads';'DisplayMode'};
data.OutputName = {'ResponseTime'; 'Cost'};

%% Identify model
comp = 1;   % Compares the data with the model
[A,B,C,D] = identify_model(data,sampling_period,comp);
sys = ss(A,B,C,D,sampling_period);
sys.InputName = data.InputName;
sys.OutputName = data.OutputName;
%sys.c(1,:) = -sys.c(1,:);

% Saving data dimensions
n = size(A,1);
m = size(B,2);
p = size(C,1);

%% Control parameters
L = 15;
q_vec = [0.006,0.0017];   if length(q_vec) ~= p, error('q_vec must be of length p'); end
r_vec = [7,1,1,1]; if length(r_vec) ~= m, error('r_vec must be of length m'); end
Q = diag(repmat(q_vec,1,L));
R = diag(repmat(r_vec,1,L));

Umin = [1,  4,0,0]';      if length(Umin) ~= m, error('Umin must be of length m'); end
Umax = [4,32,5,1]'; if length(Umax) ~= m, error('Umax must be of length m'); end
DeltaUmin = Umin - Umax;
DeltaUmax = Umax - Umin;

%% Kalman filter design
Qn = 1e0 * eye(m,m);
Rn = 1e0 * eye(p,p);

[kal,Lk,Pk] = kalman(sys,Qn,Rn);

%% Setpoint
sp = [0,0,4]';

%% Write needed matrices in Python
% out = write_python_vec(A,B,C,D,L,q_vec,r_vec,Umin,Umax,DeltaUmin,DeltaUmax,diag(Qn)',diag(Rn)',Lk,Pk,sp);
% fprintf(out);

out = write_python_fun(A,B,C,D,L,q_vec,r_vec,Umin,Umax,DeltaUmin,DeltaUmax,diag(Qn),diag(Rn),Lk,Pk,sp);

fid=fopen(file_fun,'w+');
fprintf(fid,out);
fclose(fid);

fprintf(['Saved function in ',file_fun,'\n']);
