import balancer as blc
import client as cl
import numpy as np
import matplotlib.pyplot as plt
import business as bn

#pdb = bn.ProductDB()
setpoint = np.matrix([[0.0],[0.0],[0.0],[100.0],[98.0],[5.0],[10.0]])
lb = blc.LoadBalancer(sp = setpoint)
# lb = blc.LoadBalancer()
lb.start()
x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7,x8,y8,x9,y9,x10,y10,x11,y11,x12,y12,x13,y13,x14,y14 = lb.join()

plt.subplot(7, 2, 1)
plt.plot(x1, y1, 'yo-')
plt.title('A tale of 2 subplots')
plt.ylabel('Arrivals')

plt.subplot(7, 2, 2)
axes = plt.gca()
axes.set_ylim([0,7200])
plt.plot(x2, y2, 'r.-')
plt.ylabel('Response Time')


plt.subplot(7,2,3)
plt.plot(x3,y3,'ko-')
plt.ylabel('Operational Cost')


plt.subplot(7,2,4)
axes = plt.gca()
axes.set_ylim([0,105.0])
#plt.ylim([95.0,100.0])
plt.plot(x4,y4,'ko-')
plt.ylabel('Load Page SR (%)')


plt.subplot(7, 2, 5)
plt.plot(x5, y5, 'r.-')
plt.ylabel('Revenue')

plt.subplot(7, 2, 6)
plt.plot(x6, y6, 'r.-')
plt.ylabel('Courier')

plt.subplot(7, 2, 7)
plt.plot(x7, y7, 'r.-')
plt.ylabel('Delivery SR (%)')

plt.subplot(7, 2, 8)
plt.plot(x8, y8, 'r.-')
plt.ylabel('Average Satisfaction')

plt.subplot(7, 2, 9)
plt.plot(x9, y9, 'r.-')
plt.ylabel('NoVM(k)')

plt.subplot(7, 2, 10)
plt.plot(x10, y10, 'r.-')
plt.ylabel('Active VMs')

plt.subplot(7, 2, 11)
plt.plot(x11, y11, 'r.-')
plt.ylabel('Cores / VM')

plt.subplot(7, 2, 12)
plt.plot(x12, y12, 'r.-')
plt.ylabel('Ads Displayed')

plt.subplot(7, 2, 13)
plt.plot(x13, y13, 'r.-')
plt.ylabel('Display Mode')

plt.subplot(7, 2, 14)
plt.plot(x14, y14, 'r.-')
plt.ylabel('Comparison SR')

plt.xlabel('time (s)')
plt.show()


# clients=[]
# for i in range(10):
#     clients.append(cl.Client())
# for c in clients:
#     print(c.id)
# clients.pop(2)
#
# for c in clients:
#     print(c.id)
