#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import sys
import csv

## Plot results
#  It plots the results in a unique figure
def plot_res(t,x,y,u,sp):
    plt.subplot(3,1,1)
    plt.plot(t.T,x.T)
    plt.xlabel('Time')
    plt.ylabel('States x')
    
    plt.subplot(3,1,2)
    plt.plot(t.T,y.T)
    plt.plot(t.T,sp.T,'k:')
    plt.xlabel('Time')
    plt.ylabel('Outputs y')
    
    plt.subplot(3,1,3)
    plt.plot(t.T,u.T)
    plt.xlabel('Time')
    plt.ylabel('Control u')
    umin = np.min(u)
    umax = np.max(u)
    axes = plt.gca()
    axes.set_ylim([umin*1.2,umax*1.2])
    
    plt.show()

## Plot results
#  It plots the results in a unique figure
def plot_res2(t,y,u,sp,ynames,unames):
    plt.subplot(2,1,1)
    plt.plot(t.T,y.T)
    plt.plot(t.T,sp.T,'k:')
    plt.xlabel('Time')
    plt.ylabel('Outputs y')
    plt.legend(ynames,loc='best')
    
    plt.subplot(2,1,2)
    plt.plot(t.T,u.T)
    plt.xlabel('Time')
    plt.ylabel('Control u')
    umin = np.min(u)
    umax = np.max(u)
    axes = plt.gca()
    axes.set_ylim([umin*1.2,umax*1.2])
    plt.legend(unames,loc='best')
    
    plt.show()

#Plot progress bar
def progress(val,end_val, bar_length=50):
    percent = float(val) / end_val
    hashes = '#' * int(round(percent * bar_length))
    spaces = ' ' * (bar_length - len(hashes))
    sys.stdout.write("\rProgress: [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
    sys.stdout.flush()

#log the values of all the inputs and outputs (usefull for the system identification)
def log(in1,in2,in3,in4,in5,in6,out1,out2,out3,out4,l):

    with open('output.csv', 'wb') as f2:
        #print 'writing output'                
        wr = csv.writer(f2, dialect='excel')
        headers = ['FhM','NoR','MCA','RfM','HfM','VP2','I1','I2','I3','I4']
        wr.writerow(headers)
        for i in xrange(0,l):
            wr.writerow([in1[i], \
                         in2[i], \
                         in3[i], \
                         in4[i], \
                         in5[i], \
                         in6[i], \
                         out1[i],\
                         out2[i],\
                         out3[i],\
                         out4[i]])
        f2.close()

#Prints a plot with all the indicators
def printI(I1,I2,I3,I4,l):

    time = xrange(0,l)
    plt.figure(1)
    plt.title('Outputs')
    subplot(4,1,1)
    plt.plot(time, I1, color='r', label='I1')
    plt.legend()
    subplot(4,1,2)
    plt.plot(time, I2, color='b', label='I2')
    plt.legend()
    subplot(4,1,3)
    plt.plot(time, I3, color='c', label='I3')
    plt.legend()
    subplot(4,1,4)
    plt.plot(time, I4, color='y', label='I4')
    plt.xlabel('time')
    plt.xticks(np.arange(min(time), max(time)+1, 50.0)) 
    #plt.ylabel('Indicators') 
    plt.ylim([0,210])
    
    plt.legend()
    #plt.ioff()
    #plt.show()
    return;    

def printSim(in1,in2,in3,in4,in5,in6,l):

    time = xrange(0,l)
    plt.figure(2)
    plt.title('Outputs')
    ax1=subplot(6,1,1)
    ax1.set_ylim([0,100])
    plt.plot(time, in1, color='r', label='FhM')
    plt.legend()
    ax2=subplot(6,1,2)
    ax2.set_ylim([0,6])
    plt.plot(time, in2, color='b', label='NoR')
    plt.legend()
    ax3=subplot(6,1,3)
    ax3.set_ylim([0,105])
    plt.plot(time, in3, color='c', label='MCA')
    plt.legend()
    ax4=subplot(6,1,4)
    ax4.set_ylim([0,50])
    plt.plot(time, in4, color='y', label='RfM')
    plt.legend()
    ax5=subplot(6,1,5)
    ax5.set_ylim([0,50])
    plt.plot(time, in5, color='m', label='HfM')
    plt.legend()
    ax6=subplot(6,1,6)
    ax6.set_ylim([-1,2])
    plt.plot(time, in6, color='g', label='VP2')
    plt.xlabel('time')
    plt.xticks(np.arange(min(time), max(time)+1, 50.0)) 
    #plt.ylabel('Indicators') 
    #plt.ylim([0,105])
    
    plt.legend()
    #plt.ioff()
    #plt.show()
    return;
