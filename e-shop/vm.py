import threading
import numpy as np
import simulation as sim
import random
import client as cl
import business as bn

class VirtualMachine(threading.Thread):

    contentType         = 1  # 0 text mode | 1 multimedia mode
    vmCount             = 0  # total number of VMs
    minDeploymentTime   = 1  # minimum amount of minutes for deploying a VM
    maxDeploymentTime   = 3  # maximum amount of minutes for deploying a VM
    minShutDownTime     = 1  # minimum amount of minutes for shutting down a VM
    maxShutDownTime     = 2  # maximum amount of minutes for shutting down a VM
    maxActiveSessions   = 500 # the maximum amount of active sessions allowed

    droppedRequests     = 0  # the amount of requests that couldn't be served
    minAds              = 0  # the minimum amount of ads displayed
    maxAds              = 4  # the maximum amount of ads displayed


    def __init__(self, id):
        threading.Thread.__init__(self)
        self. condition     = threading.Condition()

        self.id             = id
        self.SGN_RUN        = False
        self.Active         = False
        self.Deploying      = False
        self.ShuttingDown   = False
        self.Terminate      = False

        self.deploymentTime = random.randint(1, 2)
        self.shutDownTime   = random.randint(1, 2)
        self.deploymentClock= self.deploymentTime
        self.shutDownClock  = 0

        """         Hardware           """
        self.numCores_k       = 12  # The number of cores for a VM
        self.numCores_k_1     = 12

        self.clientQueue      = 0  # The new arrivals
        self.activeClients    = []  # The serving visitors

        """ Creating a log file to record the VM's activity """
        self.file = 'fileVM' + str(self.id) + '.log'        # A log file for each VM
        with open(self.file, "wt") as out_file:
            out_file.write('Deploying Virtual Machine ' + str(self.id) + '\n')

    def run(self):

        while not self.Terminate :
            # if self.Terminate:
            #     break
            self.condition.acquire()
            while not self.SGN_RUN and not self.Terminate:
                #print(str(self.id))
                self.condition.wait()

            """ In every step of the simulation the SGN_RUN becomes True by the
            LoadBalancer and at the end of each run becomes False again waiting
            for the next minute of the simulation                          """
            if self.SGN_RUN:  # The thread is allowed to run
                #print self.id
                """ When the VM is Deploying"""
                if self.Deploying:
                    if self.deploymentClock > 0:
                        self.deploymentClock    -= 1
                        self.SGN_RUN            = False
                        with open(self.file, "a") as out_file:
                            out_file.write('Deploying Virtual Machine ' +
                                           str(self.id) + '\n')
                        continue
                    else:
                        with open(self.file, "a") as out_file:
                            out_file.write('Deployed Virtual Machine ' +
                                           str(self.id) + '\n')
                        self.Deploying  = False
                        self.Active     = True
                """---------------------------------------------------------"""

                """ When the VM is ready to serve visitors"""
                if self.Active and (not self.ShuttingDown):
                    with open(self.file, "a") as out_file:
                        out_file.write('VM accepting clients\n')
                    """ """
                    # 1.create clients and add them to the servingClients list
                    for i in range(self.clientQueue):
                        self.activeClients.append(cl.Client())

                    self.clientQueue=0

                    k=0
                    with open(self.file, "a") as out_file:
                        out_file.write(str(len(self.activeClients))+' activeClients \n')
                    for c in self.activeClients:
                        c.browse()
                        with open(self.file, "a") as out_file:
                            out_file.write('Client '+str(c.id)+' is browsing \n')
                        if c.visitingTime==0:
                            self.activeClients.pop(k)
                            with open(self.file, "a") as out_file:
                                out_file.write('Client '+str(c.id)+'just left \n')

                        k+=1
                    # 3. process each client decrease their clock
                    #self.SGN_RUN = False  # 4. pause the vm until the next step
                if self.ShuttingDown:
                    self.Active = False
                    if self.shutDownClock > 0:
                        self.shutDownClock -= 1
                        with open(self.file, "a") as out_file:
                            out_file.write('VM is shutting down\n')
                        continue
                    else:
                        self.ShuttingDown = False
                        with open(self.file, "a") as out_file:
                            out_file.write('VM is shut down\n')
                """---------------------------------------------------------"""

                """ The thread will wait for the next minute of the simulation
                when the LoadBalancer will make SGN_RUN True again           """
                self.SGN_RUN = False
                self.condition.release()


                """---------------------------------------------------------"""
            """--------------------------------------------------------------"""
        del self

    def __del__(self):
        with open(self.file, "a") as out_file:
            out_file.write('VM is released\n')
