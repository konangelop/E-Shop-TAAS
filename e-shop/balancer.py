#!/usr/bin/python

import threading
import numpy as np
import argparse
import os
import sys
import simulation as sim
import vm
import random
import math
import business as bn
import client as cl
import socket
import csv
import time
import zanshin

# My libraries
import libs.mpyc as reg
import libs.init_mpc as inp
import libs.utils as ut


class LoadBalancer(threading.Thread):
    # The server pool of replicated Virtual Machines
    vmPool = [vm.VirtualMachine(1), vm.VirtualMachine(2), vm.VirtualMachine(3),
              vm.VirtualMachine(4), vm.VirtualMachine(5), vm.VirtualMachine(6),
              vm.VirtualMachine(7), vm.VirtualMachine(8), vm.VirtualMachine(9),
              vm.VirtualMachine(10), vm.VirtualMachine(
                  11), vm.VirtualMachine(12),
              vm.VirtualMachine(13), vm.VirtualMachine(14)]

    # Control parameters
    NoVM_k = 8  # NoVM(k)
    NoVM_k_1 = 8  # NoVM(k+1)
    activeVMnum_k = 8
    activeVMnum_k_1 = 8
    cores_k = 12
    cores_k_1 = 12
    ads_k = 3
    ads_k_1 = 3
    displayMode = 0  # 0 textual mode | 1 multimedia mode

    dVM = 0
    ComparisonSuccess = 0
    timesMultimedia = 1
    timesTextual = 1

    # Input Logs
    vmLog = []
    activeVMLog = []
    coresLog = []
    adsLog = []
    displayModeLog = []

    # Output Logs
    rt = []
    operationalCost = []
    requestsSuccessRate = []
    dropsLog = []
    totalArrivalsLog = []
    ComparisonSuccessLog = []
    courierLog = []

    def __init__(self, sp=np.zeros((7, 1))):

        threading.Thread.__init__(self)
        print('E-Shop is deploying...')

        self.responseTime = 0

        for i in range(len(LoadBalancer.vmPool)):
            LoadBalancer.vmPool[i].start()

        """ Adding the initial 8 VMs """
        LoadBalancer.vmPool[0].Active = True
        LoadBalancer.vmPool[1].Active = True
        LoadBalancer.vmPool[2].Active = True
        LoadBalancer.vmPool[3].Active = True
        LoadBalancer.vmPool[4].Active = True
        LoadBalancer.vmPool[5].Active = True
        LoadBalancer.vmPool[6].Active = True
        LoadBalancer.vmPool[7].Active = True
        LoadBalancer.vmPool[8].Active = False
        LoadBalancer.vmPool[9].Active = False
        LoadBalancer.vmPool[10].Active = False
        LoadBalancer.vmPool[11].Active = False
        LoadBalancer.vmPool[12].Active = False
        LoadBalancer.vmPool[13].Active = False

        self.visitors_k = 1
        self.visitors_k_1 = 1

        self.responseTime_k = 120  # RT(k)
        self.responseTime_k_1 = 120  # RT(k-1)
        self.responseTime_k_2 = 120  # RT(k-2)

        self.totalArrivals = 0

        self.cost               = LoadBalancer.NoVM_k * 2 +\
                                LoadBalancer.cores_k * 0.12 * LoadBalancer.NoVM_k

        self.SGN_SYSID =         True
        self.ZanshinAdaptation = False
        self.MPC_Adaptation = False

        # Return value
        self._return = None

        # Adding MPC object
        # Initialize matrices for MPC
        A, B, C, D, L, Q, R, Umin, Umax, DeltaUmin, DeltaUmax, Qn, Rn, Lk, Pk, sp = inp.initialize_mpc();
        # Initializing the controller
        self.controller = reg.MPCController(A, B, C, D,
                                L, Q, R,
                                Lk, Pk, Qn, Rn,
                                Umin, Umax, DeltaUmin, DeltaUmax,
                                optim=1,
                                fast=0,
                                time_varying=1)
        # self.controller.initialize_controller_state(np.matrix([[1.0],[2]]))
        self.sp = sp

    def join(self):
        threading.Thread.join(self)
        return self._return

    def run(self):
        increasingVM = True
        increasingCores = True
        increasingAds = True
        increasingMode = True
        hours = 1
        revenueHourly = 1
        print("E-Shop is deployed")
        print("Simulation is running")

        dCores_k_1 = 0  # dCores(k-1)
        actualdVM_k_1 = 0  # actualdVM(k-1)

        while sim.clock < sim.simTime:
            # print 'Balancer is running'

            flag = True

            while(flag):
                flag = False
                for i in range(len(LoadBalancer.vmPool)):
                    if LoadBalancer.vmPool[i].SGN_RUN:
                        flag = True
                        break
                time.sleep(0.01)

            """---Distributing visitors to Active VMs---"""
            self.distributeClients(sim.arrivals[sim.clock])
            """---------------------------------------"""

            for i in range(len(LoadBalancer.vmPool)):
                LoadBalancer.vmPool[i].condition.acquire()
                LoadBalancer.vmPool[i].SGN_RUN = True
                LoadBalancer.vmPool[i].condition.notify()
                LoadBalancer.vmPool[i].condition.release()

            """---------------MONITORING--------------"""

            ##########################Delta calculations#######################
            actualdVM = LoadBalancer.activeVMnum_k_1 - LoadBalancer.activeVMnum_k
            dcores = LoadBalancer.cores_k_1 - LoadBalancer.cores_k
            dads = LoadBalancer.ads_k_1 - LoadBalancer.ads_k
            dvisitors = self.visitors_k_1 - self.visitors_k

            dVM_contribution = self.visitors_k * actualdVM * 11.8
            dcores_contribution = self.visitors_k * dcores * 0.93
            dads_contribution = dvisitors * ((dads * 3)**3) * 366 / (
                (0.6 * (LoadBalancer.activeVMnum_k**2) + 0.5 * LoadBalancer.cores_k)**2)
            ###################################################################

            # Update control parameter values
            LoadBalancer.NoVM_k = LoadBalancer.NoVM_k_1
            LoadBalancer.cores_k = LoadBalancer.cores_k_1
            LoadBalancer.ads_k = LoadBalancer.ads_k_1
            self.visitors_k = self.visitors_k_1
            self.responseTime_k_2 = self.responseTime_k_1
            self.responseTime_k_1 = self.responseTime_k
            dCores_k_1 = dcores
            actualdVM_k_1 = actualdVM

            LoadBalancer.activeVMnum_k = LoadBalancer.activeVMnum_k_1

            if self.responseTime_k_1 < 200:
                self.responseTime_k_1 = 200

            if self.responseTime_k_1 > 7000:
                self.responseTime_k_1 = 7000

            # a1 = 0.6*(self.visitors_k*(21+9*LoadBalancer.displayMode+ \
            #                            12*LoadBalancer.ads_k)/(0.32*(LoadBalancer.NoVM_k)**3+\
            #                                                    0.11*(LoadBalancer.cores_k)**2))
            ############
            a1b = 0.6 * (self.visitors_k * (191 + 32 * LoadBalancer.displayMode +
                                       23 * LoadBalancer.ads_k) / (0.22 * (LoadBalancer.NoVM_k)**3 +
                                                               0.1 * (LoadBalancer.cores_k)**2))
            ###########
            a2 = 0.3 * self.responseTime_k_1 + 0.1 * self.responseTime_k_2
            a3 = (dvisitors)**3 / (((LoadBalancer.NoVM_k) **
                  3 + 0.2 * (LoadBalancer.cores_k)**2))**4
            a4 = 425 * (actualdVM / self.visitors_k * 7.12)
            a5 = actualdVM * 16713 / (0.014 * (self.visitors_k)**2)
            a6 = 67645 * (actualdVM)**3 / (0.0012 * (self.visitors_k)**3)
            #######

            ########
            a7 = 47265 * (actualdVM)**3 / (0.0002 * (self.visitors_k)**4)
            a8 = 172656 * (actualdVM)**3 / (0.0001 * (self.visitors_k)**4)
            a9 = 67645 * actualdVM_k_1 / (0.002 * (self.visitors_k)**3)
            a10 = 1975 * (actualdVM_k_1)**3 / (0.0012 * (self.visitors_k)**4)
            a11 = 175 * math.pow(actualdVM_k_1, 3) / \
                                 (0.0002 * (self.visitors_k)**3)
            # a12 = dcores*1.3/(0.014*(self.visitors_k)**2)
            # a13 = 3.5*(dcores)**3/(0.012*(self.visitors_k)**3)
            # a14 = 6.5*(dcores)**3/(0.002*(self.visitors_k)**4)
            # a15 = 2.65*dCores_k_1/(0.02*(self.visitors_k)**3)
            # a16 = 0.75*(dCores_k_1)**3/(0.0012*(self.visitors_k)**4)
            # ###
            a12b = dcores * 1.8 / (0.013 * (self.visitors_k)**2)
            a13b = 3.7 * (dcores)**3 / (0.012 * (self.visitors_k)**3)
            a14b = 6.9 * (dcores)**3 / (0.002 * (self.visitors_k)**4)
            a15b = 2.85 * dCores_k_1 / (0.02 * (self.visitors_k)**3)
            a16b = 0.85 * (dCores_k_1)**3 / (0.0012 * (self.visitors_k)**4)

            self.responseTime_k = a1b + a2 + a3 - a4 - a5 - a6 - a7 - a8 - a9 - a10 -\
            a11 - a12b - a13b - a14b - a15b - a16b

            ###################################################################

            # Logging resonse time measurements
            LoadBalancer.rt.append(self.responseTime_k)

            # Calculating the operation cost
            self.cost = LoadBalancer.NoVM_k * 20 + \
                LoadBalancer.cores_k * 1.2 * LoadBalancer.NoVM_k
            LoadBalancer.operationalCost.append(self.cost)

            # Measuring the total arrivals until the moment k
            self.totalArrivals += sim.arrivals[sim.clock]
            loadSuccess = 100 * \
                (self.totalArrivals - vm.VirtualMachine.droppedRequests) / \
                 self.totalArrivals

            # Calulate the success rate of loading the e-shop's page
            LoadBalancer.requestsSuccessRate.append(math.ceil(loadSuccess))

            # Logging the dropped requests
            LoadBalancer.dropsLog.append(vm.VirtualMachine.droppedRequests)

            # Logging the total number of visitors
            LoadBalancer.totalArrivalsLog.append(self.totalArrivals)

            # Logging the revenue/hour
            if sim.clock % 60 == 0:
                hours += 1
                revenueHourly = bn.revenue / hours
            bn.revenueLog.append(revenueHourly)

            # Delivery Success Rate calculation
            if bn.totalPurchases > 0:
                bn.deliverySuccessRate = 100 * bn.totalSuccessfulDeliveries / bn.totalPurchases
            bn.deliverySuccessRateLog.append(bn.deliverySuccessRate)

            # satisfaction calculation
            if bn.totalPurchases > 0:
                cl.Client.averageSatisfaction = cl.Client.totalCustomerSatisfaction / bn.totalPurchases
            cl.Client.averageSatisfactionLog.append(
                cl.Client.averageSatisfaction)

            # Calculate the success rate of multimedia is 10 times more used
            # than textual content
            if LoadBalancer.displayMode == 1:
                LoadBalancer.timesMultimedia += 1
            else:
                LoadBalancer.timesTextual += 1

            LoadBalancer.ComparisonSuccessLog.append(
                math.ceil(LoadBalancer.timesMultimedia / LoadBalancer.timesTextual))

            # Logging Control Parameters
            LoadBalancer.vmLog.append(LoadBalancer.NoVM_k)
            LoadBalancer.activeVMLog.append(LoadBalancer.activeVMnum_k)
            LoadBalancer.coresLog.append(LoadBalancer.cores_k)
            LoadBalancer.adsLog.append(LoadBalancer.ads_k)
            LoadBalancer.displayModeLog.append(LoadBalancer.displayMode)
            LoadBalancer.courierLog.append(bn.courier)

            """---------------------------------------"""

            """---------Adaptation Actions--------------"""
            u_k = [LoadBalancer.NoVM_k,
                   LoadBalancer.cores_k,
                   LoadBalancer.ads_k,
                   LoadBalancer.displayMode]

            y_k = [self.responseTime_k,             \
                 #  1/(revenueHourly),            \
                   self.cost]  # \
                  # math.ceil(loadSuccess),          \
                  # bn.deliverySuccessRate,          \
                   # cl.Client.averageSatisfaction,   \
                   # math.ceil(LoadBalancer.timesMultimedia/LoadBalancer.timesTextual)

            u_k_1 = u_k

            if sim.clock % 8 == 0:


                if self.SGN_SYSID: #System Identification Block

                    u_k_1[0]   = random.randint(4,14)
                    u_k_1[1] = random.randint(8,64)
                    u_k_1[2] = random.randint(0,5)
                    u_k_1[3] = random.randint(0,1)


                    LoadBalancer.dVM = LoadBalancer.NoVM_k_1-LoadBalancer.NoVM_k
                    #print('dVM = '+str(LoadBalancer.dVM))
                    if LoadBalancer.dVM>0:
                        self.addVM(LoadBalancer.dVM)
                    elif LoadBalancer.dVM<0:
                        self.removeVM(abs(LoadBalancer.dVM))

                elif self.ZanshinAdaptation:
                    u_k_1 = zanshin.control(u_k,y_k)

                elif self.MPC_Adaptation:
                    yy = np.matrix(y_k).T
                    #print yy
                    uu = self.controller.compute_u(yy,self.sp)
                    u_k_1 = [uu.item(i) for i in range(0,len(uu))]


                LoadBalancer.NoVM_k_1   = int(u_k_1[0])
                LoadBalancer.cores_k_1  = int(u_k_1[1])
                LoadBalancer.ads_k_1    = int(u_k_1[2])
                # bn.courier              = u_k_1[3]
                LoadBalancer.displayMode= int(u_k_1[3])


                LoadBalancer.dVM = LoadBalancer.NoVM_k_1-LoadBalancer.NoVM_k
                # print('dVM = '+str(LoadBalancer.dVM)+' active vm = '+str(LoadBalancer.activeVMnum_k)+' NoVM = '+str(LoadBalancer.NoVM_k))
                if LoadBalancer.dVM>0:
                    self.addVM(LoadBalancer.dVM)
                elif LoadBalancer.dVM<0:
                    self.removeVM(abs(LoadBalancer.dVM))
            #############################

            # count the visitor that will be browsing in k+1
            self.visitors_k_1=1
            for i in range(len(LoadBalancer.vmPool)):
                if LoadBalancer.vmPool[i].Active and not LoadBalancer.vmPool[i].Deploying:
                    self.visitors_k_1+=len(LoadBalancer.vmPool[i].activeClients)

            # Find how many VMs will be active in the next step
            LoadBalancer.activeVMnum_k_1 = 0
            for i in range(len(LoadBalancer.vmPool)):
                if LoadBalancer.vmPool[i].Active and not LoadBalancer.vmPool[i].Deploying and not LoadBalancer.vmPool[i].ShuttingDown:
                    LoadBalancer.activeVMnum_k_1+=1

            if LoadBalancer.activeVMnum_k_1 == 0:
                print('ERROR ZERO ACTIVE VM')


            """-----------------------------------------"""
            sim.clock += 1

        for i in range(len(LoadBalancer.vmPool)):
            LoadBalancer.vmPool[i].condition.acquire()
            LoadBalancer.vmPool[i].Terminate = True
            LoadBalancer.vmPool[i].condition.notify()
            LoadBalancer.vmPool[i].condition.release()

        flag = True
        while flag:
            flag=False
            for i in range(len(LoadBalancer.vmPool)):
                if LoadBalancer.vmPool[i].isAlive():
                    flag=True
                    break
            # print('&&&&')
            time.sleep(0.01)

        for i in range(len(LoadBalancer.vmPool)):
            LoadBalancer.vmPool[i].join()

        print("Dropped "+str(vm.VirtualMachine.droppedRequests)+" requests")
        print("Simulation finished")



        in1     = LoadBalancer.vmLog
        in2     = LoadBalancer.coresLog
        in3     = LoadBalancer.adsLog
        in4     = LoadBalancer.courierLog
        in5     = LoadBalancer.displayModeLog
        out1    = LoadBalancer.rt
        out2    = bn.revenueLog
        out3    = LoadBalancer.operationalCost
        out4    = LoadBalancer.requestsSuccessRate
        out5    = bn.deliverySuccessRateLog
        out6    = cl.Client.averageSatisfactionLog
        out7    = LoadBalancer.ComparisonSuccessLog
        l       = len(sim.arrivals)

        l-=1
        self.logger(in1,in2,in3,in4,in5,out1,out2,out3,out4,out5,out6,out7,l)


        x1 = np.arange(0, len(sim.arrivals), 1)
        x2 = np.arange(0, len(LoadBalancer.rt),1)
        x3 = np.arange(0, len(LoadBalancer.operationalCost),1)
        x4 = np.arange(0, len(LoadBalancer.requestsSuccessRate),1)
        x5 = np.arange(0, len(bn.revenueLog), 1)
        x6 = np.arange(0, len(LoadBalancer.courierLog),1)
        x7 = np.arange(0, len(bn.deliverySuccessRateLog),1)
        x8 = np.arange(0, len(cl.Client.averageSatisfactionLog),1)
        x9 = np.arange(0, len(LoadBalancer.vmLog),1)
        x10 = np.arange(0, len(LoadBalancer.activeVMLog),1)
        x11 = np.arange(0, len(LoadBalancer.coresLog),1)
        x12 = np.arange(0, len(LoadBalancer.adsLog),1)
        x13 = np.arange(0, len(LoadBalancer.displayModeLog),1)
        x14 = np.arange(0, len(LoadBalancer.ComparisonSuccessLog),1)

        y1 = sim.arrivals
        y2 = LoadBalancer.rt
        y3 = LoadBalancer.operationalCost
        y4 = LoadBalancer.requestsSuccessRate
        y5 = bn.revenueLog
        y6 = LoadBalancer.courierLog
        y7 = bn.deliverySuccessRateLog
        y8 = cl.Client.averageSatisfactionLog
        y9 = LoadBalancer.vmLog
        y10 = LoadBalancer.activeVMLog
        y11 = LoadBalancer.coresLog
        y12 = LoadBalancer.adsLog
        y13 = LoadBalancer.displayModeLog
        y14 = LoadBalancer.ComparisonSuccessLog

        self._return = x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7,x8,y8,x9,y9,x10,y10,x11,y11,x12,y12,x13,y13,x14,y14


    # this method finds the VM with the smallest amount of active clients
    def findMinQueue(self):

        counter = len(LoadBalancer.vmPool)
        i = 0
        min = 100000
        while (counter > 0):
            if(LoadBalancer.vmPool[i].Active and len(LoadBalancer.vmPool[i].activeClients)+ LoadBalancer.vmPool[i].clientQueue < min):
                min = len(LoadBalancer.vmPool[i].activeClients)+LoadBalancer.vmPool[i].clientQueue
                global minVM
                minVM = i
            i += 1
            counter -= 1

        for j in range(1, len(LoadBalancer.vmPool)):
            if len(LoadBalancer.vmPool[j].activeClients)+LoadBalancer.vmPool[j].clientQueue < min and LoadBalancer.vmPool[j].Active:
                min = len(LoadBalancer.vmPool[j].activeClients)+LoadBalancer.vmPool[j].clientQueue
                minVM = j

        return minVM

    def distributeClients(self, num):
        while(num > 0):
            selectedVM = self.findMinQueue()
            if len(LoadBalancer.vmPool[selectedVM].activeClients) + LoadBalancer.vmPool[selectedVM].clientQueue < vm.VirtualMachine.maxActiveSessions:
                LoadBalancer.vmPool[selectedVM].clientQueue += 1
            else:
                vm.VirtualMachine.droppedRequests += 1

            num -= 1


    def addVM(self, num):
        counter = num
        # LoadBalancer.NoVM_k_1+=num
        for i in range( len(LoadBalancer.vmPool)):
            if counter == 0:
                break
            if not LoadBalancer.vmPool[i].Active and not LoadBalancer.vmPool[i].Deploying and not LoadBalancer.vmPool[i].ShuttingDown:
                    LoadBalancer.vmPool[i].condition.acquire()
                    LoadBalancer.vmPool[i].Deploying = True
                    LoadBalancer.vmPool[i].SGN_RUN = True
                    LoadBalancer.vmPool[i].deploymentClock = LoadBalancer.vmPool[i].deploymentTime
                    LoadBalancer.vmPool[i].condition.notify()
                    LoadBalancer.vmPool[i].condition.release()
                    # LoadBalancer.vmPool[i].SGN_RUN = True
                    counter-=1

    def removeVM(self, num):
        counter = num
        for i in range( len(LoadBalancer.vmPool)):
            if counter == 0:
                break
            if  LoadBalancer.vmPool[i].Active and not LoadBalancer.vmPool[i].Deploying and not LoadBalancer.vmPool[i].ShuttingDown:
                LoadBalancer.vmPool[i].condition.acquire()
                LoadBalancer.vmPool[i].Active = False
                LoadBalancer.activeClients = []
                LoadBalancer.vmPool[i].SGN_RUN = False
                LoadBalancer.vmPool[i].condition.notify()
                LoadBalancer.vmPool[i].condition.release()
                counter-=1

    def logger(self,in1,in2,in3,in4,in5,out1,out2,out3,out4,out5,out6,out7,l):

        with open('output.csv', 'wb') as f2:
            # print 'writing output'
            wr = csv.writer(f2, dialect='excel')
            headers = ['VM','Cores','Ads','Courier','Display Mode','Response Time','Revenue','Cost','Load SR','Delivery SR', 'Customer Sat', 'Comparison SR']
            wr.writerow(headers)
            for i in range(0,l):
                wr.writerow([in1[i], \
                             in2[i], \
                             in3[i], \
                             in4[i], \
                             in5[i], \
                             out1[i],\
                             out2[i],\
                             out3[i],\
                             out4[i],\
                             out5[i],\
                             out6[i],\
                             out7[i]])
            f2.close()

    def __del__(self):
        print("System is shutting down")
