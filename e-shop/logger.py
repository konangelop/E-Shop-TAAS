import time
import socket
import logging
import random

class ExperimentLogger:
    def __init__(self,filename='log.csv',Ts=60,Tfin=100*60,mode='id'):
        # Logging information
        self.FORMAT = '%(message)s';
        logging.basicConfig(filename=filename,\
                            format=self.FORMAT,\
                            filemode='w',\
                            level=logging.INFO)
        logging.info('Time,\
                      Dimmer,\
                      Servers,\
                      ActiveServers,\
                      Basic_rt,\
                      Opt_rt,\
                      Basic_throughput,\
                      Opt_throughput,\
                      ArrivalRate,\
                      uDeltaServers,\
                      uServers,\
                      uDimmer')

        # Mode
        self.mode = mode

        # Sampling time in seconds
        self.Ts = Ts
        self.Tfin = Tfin

        # Connect to socket
        self.s =socket.socket()
        host = socket.gethostname()
        port = 4242

        self.s.connect((host,port))

        # initialize other variables
        self.dimmer          = 0.0
        self.servers         = 0
        self.activeServers   = 0
        self.basicRT         = 0.0
        self.optRT           = 0.0
        self.basicThroughput = 0.0
        self.optThroughput   = 0.0
        self.arrivalRate     = 0.0

        # control variables
        self.uDeltaServers   = 0
        self.uServers        = 0
        self.uDimmer         = 0.0

        # Other auxiliary variables
        self.maxServers = 3
        self.minServers = 1
        self.dimmerRange = [x for x in self.drange(0.0,1.0,0.1)]

    def drange(self,start,stop,step):
        r = start
        while r < stop:
            yield r
            r+= step


    def sense(self):
        # Get dimmer
        self.s.send('get_dimmer')
        self.dimmer = float(self.s.recv(1024).rstrip('\n'))

        # Get servers
        self.s.send('get_servers')
        self.servers = (int(self.s.recv(1024).rstrip('\n')))
        
        # Get active servers
        self.s.send('get_active_servers')
        self.activeServers = int(self.s.recv(1024).rstrip('\n'))

        # Get basic response time
        self.s.send('get_basic_rt')
        self.basicRT = float(self.s.recv(1024).rstrip('\n'))

        # Get opt response time
        self.s.send('get_opt_rt')
        self.optRT = float(self.s.recv(1024).rstrip('\n'))

        # Get basic thorughput
        self.s.send('get_basic_throughput')
        self.basicThroughput = float(self.s.recv(1024).rstrip('\n'))

        # Get opt response time
        self.s.send('get_opt_throughput')
        self.optThroughput = float(self.s.recv(1024).rstrip('\n'))

        # Get arrival rate
        self.s.send('get_arrival_rate')
        self.arrivalRate = float(self.s.recv(1024).rstrip('\n'))


    def actuate_id(self):
        # Change server actuation
        if self.servers == self.activeServers: # Check if all the servers that should be active are active
            # Sanity conditions for adding or removing servers
            if self.servers >= self.maxServers:
                server_actuation = random.choice([-1,0])
            elif self.servers <= self.minServers:
                server_actuation = random.choice([0,1])
            else:
                server_actuation = random.choice([-1,0,1])

            self.uDeltaServers = server_actuation
            self.uServers      = self.servers + server_actuation
            #Implement actuation
            if server_actuation == -1:
                print('Actuation: Removing a server...')
                self.s.send('remove_server')
                self.s.recv(1024)

            if server_actuation == 1:
                print('Actuation: Adding a server...')
                self.s.send('add_server')
                self.s.recv(1024)

        # Change dimmer actuation
        dimmer_actuation = random.choice(self.dimmerRange)
        dimmer_actuation = min(max(dimmer_actuation,0.1),0.9)
        self.uDimmer = dimmer_actuation
        if dimmer_actuation != self.dimmer:
            print('Actuation: Setting dimmer to {}'.format(dimmer_actuation))
            self.s.send('set_dimmer {}'.format(dimmer_actuation))
            self.s.recv(1024)

    def actuate(self):
        ##
        # TODO: Modify this part to actuate the MPC controller
        ##
        # Change server actuation
        if self.servers == self.activeServers: # Check if all the servers that should be active are active
            # Sanity conditions for adding or removing servers
            if self.servers >= self.maxServers:
                server_actuation = random.choice([-1,0])
            elif self.servers <= self.minServers:
                server_actuation = random.choice([0,1])
            else:
                server_actuation = random.choice([-1,0,1])

            #Implement actuation
            if server_actuation == -1:
                print('Actuation: Removing a server...')
                self.s.send('remove_server')
                self.s.recv(1024)

            if server_actuation == 1:
                print('Actuation: Adding a server...')
                self.s.send('add_server')
                self.s.recv(1024)
        else:
            server_actuation = 0

        # Storing results
        self.uDeltaServers = server_actuation
        self.uServers = server_actuation + self.servers

        # Change dimmer actuation
        dimmer_actuation = random.choice(self.dimmerRange)
        dimmer_actuation = min(max(dimmer_actuation,0.1),0.9)
        self.uDimmer = dimmer_actuation
        if dimmer_actuation != self.dimmer:
            print('Actuation: Setting dimmer to {}'.format(dimmer_actuation))
            self.s.send('set_dimmer {}'.format(dimmer_actuation))
            self.s.recv(1024)


    def run(self):
        init_time = time.time()
        past_timestamp = time.time()
        present_timestamp = time.time()

        while time.time() - init_time<=self.Tfin: # runs for Tfin seconds
            present_timestamp = time.time()
            if present_timestamp - past_timestamp>=self.Ts: # sampling time
                
                past_timestamp = present_timestamp
                print('Monitoring {}...'.format(time.asctime()))
                
                # Sense
                self.sense()

                # Actuate
                if self.mode == 'id':
                    self.actuate_id()
                else:
                    self.actuate()

                # Add this to the log
                logging.info('%f,%g,%d,%d,%g,%g,%g,%g,%g,%d,%d,%g',\
                             past_timestamp,\
                             self.dimmer,\
                             self.servers,\
                             self.activeServers,\
                             self.basicRT,\
                             self.optRT,\
                             self.basicThroughput,\
                             self.optThroughput,\
                             self.arrivalRate,\
                             self.uDeltaServers,\
                             self.uServers,\
                             self.uDimmer)
                


        self.s.close()
        print('Completed!')



if __name__ == "__main__":

    el = ExperimentLogger(Ts = 60, Tfin = 6180, mode='id')
    el.run()



