import math
import random
import business as bn
import balancer as bl

class Client:
    clientID=0
    totalCustomerSatisfaction=0;
    averageSatisfaction = 0
    averageSatisfactionLog=[]


    def __init__(self):
        self.visitingTime = random.randint(1,6)
        self.id = Client.clientID
        Client.clientID+=1


    def browse(self):
        self.visitingTime-=1
        self.buys()

    def buys(self):
        yesPr = int(math.ceil(0.3+0.7*bl.LoadBalancer.ads_k+2*bl.LoadBalancer.displayMode))
        noPr = 100 - yesPr
        picks = yesPr*['Yes'] + noPr*['No']
        result = random.choice(picks)
        if result == 'Yes':
            if len(bn.ProductDB.productList)>=1:
                p = random.choice(bn.ProductDB.productList) #select product to buy
            else:
                p = bn.Product(9999,100)
            bn.totalPurchases+=1 # increase the number of overall purchases
            bn.revenue += p.price # increase revenue by the products price

            if bn.courier==1:
                c1Pr=90
                c2Pr=5
                c3Pr=5
            elif bn.courier==2:
                c1Pr=5
                c2Pr=90
                c3Pr=5
            else:
                c1Pr=5
                c2Pr=5
                c3Pr=90

            courierPicks = c1Pr*['c1']+c2Pr*['c2']+c3Pr*['c3']
            selectedCourier = random.choice(courierPicks)
            if self.isDelivered(selectedCourier):
                # increase by 1 successful purchases
                bn.totalSuccessfulDeliveries+=1
                #find out in how many days
                days = self.countDays(selectedCourier)
                self.rate(days)
            #if the product is not deliveredthe customer satisfaction is 0
            #so no actions needed

    def rate(self,days):
        if days<=4:
            Client.totalCustomerSatisfaction+=5
        elif days<=5:
            Client.totalCustomerSatisfaction+=random.randint(4,5)
        elif days<=6:
            Client.totalCustomerSatisfaction+=random.randint(3,5)
        elif days<=8:
            Client.totalCustomerSatisfaction+=random.randint(2,4)
        else:
            Client.totalCustomerSatisfaction+=random.randint(1,4)


    def countDays(self,courierName):
        if courierName == 'c1':
            return random.randint(bn.avgDeliveryTimeCourier1-2,bn.avgDeliveryTimeCourier1+1)
        elif courierName =='c2':
            return random.randint(bn.avgDeliveryTimeCourier2-2,bn.avgDeliveryTimeCourier2+1)
        else:
            return random.randint(bn.avgDeliveryTimeCourier3-2,bn.avgDeliveryTimeCourier3+1)

    def isDelivered(self,courierName):
        #Find out if the order is delivered
        if courierName == 'c1':
            yesPr = int(bn.reliabilityCourier1*100)
            noPr  = 100 - yesPr
            bn.revenue-=bn.costCourier1 #the cost to the eshop for courier 1
        elif courierName == 'c2':
            yesPr = int(bn.reliabilityCourier2*100)
            noPr  = 100 - yesPr
            bn.revenue-=bn.costCourier2 #the cost to the eshop for courier 2
        else:
            yesPr = int(bn.reliabilityCourier3*100)
            noPr  = 100 - yesPr
            bn.revenue-=bn.costCourier3 #the cost to the eshop for courier 3

        picks = yesPr*['Yes']+noPr*['No']
        result= random.choice(picks)
        if result == 'Yes':
            return True
        else:
            return False
