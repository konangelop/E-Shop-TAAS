import random
import numpy as np
import scipy as sp
import scipy.stats
import math
import matplotlib.pyplot as plt
import csv

class Generator:

    def addArrivals(self):
        uni = sp.stats.uniform(    self.loc, self.scale)
        arrivals = uni.rvs()
        self.traffic.append(math.ceil(arrivals))


    def increaseTraffic(self, step, period):
        for i in range(period):
            self.loc =     self.loc + step
            self.scale += step
            self.addArrivals()

    def decreaseTraffic(self, step, period):
        for i in range(period):
            self.loc -= step
            self.scale -= step
            if self.loc < 0:
                self.loc=0
                self.scale=4
            self.addArrivals()

    #def decreaseTraffic(self, x):
    #        self.loc -= x

    # def maintainTraffic(self,period):
    #     self.traffic.extend(np.random.uniform(low=self.min,high=self.max,size=period))

    # def increaseTraffic(self,period):
    #     count = period
    #     while count > 0:
    #         self.min+=1
    #         self.max

    def createTraffic(self):
        # Low traffic 0-300 min
        for i in range(300):
            self.addArrivals()

        # increase by 3 the arrival rate for every min  | 301-480
        self.increaseTraffic(1, 30)
        for i in range(60):
            self.addArrivals()
        self.decreaseTraffic(1,20)
        for i in range(120):
            self.addArrivals()
        self.increaseTraffic(1, 10)
        for i in range(90):
            self.addArrivals()
        self.decreaseTraffic(1,25)
        for i in range(90):
            self.addArrivals()
        self.increaseTraffic(1,30)
        for i in range(300):
            self.addArrivals()
        self.decreaseTraffic(1,20)
        for i in range(300):
            self.addArrivals()
        self.increaseTraffic(1, 20)
        for i in range(60):
            self.addArrivals()
        self.decreaseTraffic(1,20)
        for i in range(120):
            self.addArrivals()
        self.increaseTraffic(1, 10)
        for i in range(290):
            self.addArrivals()
        self.decreaseTraffic(1,20)
        for i in range(390):
            self.addArrivals()
        self.increaseTraffic(1, 10)
        for i in range(90):
            self.addArrivals()
        self.decreaseTraffic(1,25)
        for i in range(90):
            self.addArrivals()
        self.increaseTraffic(1,30)
        for i in range(300):
            self.addArrivals()
        self.decreaseTraffic(1,20)
        for i in range(300):
            self.addArrivals()
        self.increaseTraffic(1, 20)
        for i in range(60):
            self.addArrivals()



        # Open File


        # t = np.arange(0, len(self.traffic), 1)
        # s = self.traffic
        # plt.plot(t, s)
        #
        # plt.xlabel('time (s)')
        # plt.ylabel('arrivals')
        # plt.title('Workload')
        # plt.show()



    def __init__(self):
        print("Simulation starting")
        self.traffic = []
        self.duration = 480  # minutes aka 1 day
        self.scale = 4
        self.loc = 0
        self.min = 0
        self.max = 3
        self.createTraffic()
